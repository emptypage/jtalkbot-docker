FROM emptypage/open_jtalk:20.4_1.11
RUN set -x && \
    apt-get -y update && \
    apt-get -y install libopus-dev python3-pip && \
    apt-get -y clean && rm -rf /var/lib/apt/lists/*
RUN set -x && \
    pip3 install jtalkbot==0.6.1.3
COPY jtalkbot-config.json /root
WORKDIR /root
CMD jtalkbot --help
