# Docker image to run jtalkbot Discord bot

Discord 読み上げボット jtalkbot の Docker イメージです。

- [Project Home Page (Bitbucket)](https://bitbucket.org/emptypage/jtalkbot-docker/src/master/)
- [emptypage/jtalkbot (Docker Hub)](https://hub.docker.com/r/emptypage/jtalkbot)
- [jtalkbot (PyPI)](https://pypi.org/project/jtalkbot/)

## 使いかた

### jtalkbot の実行

ターミナルで `docker run --rm emptypage/jtalkbot` を実行するとイメージにインストールされた `jtalkbot` コマンドのヘルプが表示されます。実際に使用するときは `jtalkbot` コマンドをオプションを付けて実行してください。

イメージは実行時のカレントディレクトリ（`/root`）に最低限の設定を書いた `jtalkbot-config.json` ファイルを持っていますが、Discord ボットのトークンだけはボット開発者が個別に持つ秘密情報のためダミーの値になっています。このため、ボットの稼働には最低でも `--token`オプションが必要です。

```
% docker run --rm emptypage/jtalkbot jtalkbot --token XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
```

その他のオプションについては [jtalkbot](https://pypi.org/project/jtalkbot/) のページを参照してください。

### 読み上げボット開発の環境として

イメージは Ubuntu に日本語 TTS (text-to-speech) プログラム open_jtalk、ボットスクリプト実行環境の Python と Discord ボット製作用のライブラリ [discord.py](https://pypi.org/project/discord.py/) が含まれたものとなっていますので、Python で discord ボットを開発、実行するときのベースイメージとしてもお使いいただけます。
